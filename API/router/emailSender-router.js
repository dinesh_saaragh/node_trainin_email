const router = require("express").Router();
const {sendEmail}=require('../controller/emailSender.controller');
router.post('/send-email',sendEmail);

module.exports = router;
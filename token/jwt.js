
const jwt = require("jsonwebtoken");
require("dotenv").config();
module.exports = {
  validateToken: (req, res, next) => {
    let token = req.get("authorization");
    if (token) {
      jwt.verify(token, process.env.ACCESS_TOKEN, (err, decoded) => {
        if (err) {
          return res.json({
            success: err,
            message: "Invalid Token..."
          });
        } else {
          req.decoded = decoded;
          next();
        }
      });
    } else {
      return res.json({
        success: 0,
        message: "Access Denied! Unauthorized User,"
      });
    }
  }
};

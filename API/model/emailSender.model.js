const mongoose=require('mongoose');

    const emailSchema=mongoose.Schema({
     
        email:{
            type:String,
            unique: true,
            required: [true, "can't be empty"]
        },
        message:{
            type:String,
            required: [true, "can't be empty"]
        }
    })
  

    module.exports=mongoose.model('email',emailSchema,'email')
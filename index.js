
const express = require("express");
const app = express();
const mongoose = require('mongoose');
require("dotenv").config();
const ejs = require('ejs');
const path = require('path');

mongoose.connect(process.env.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    next();
  });
app.use(express.json())
const register = require('./API/router/emailSender-router');
app.use(register)
// Initialising Express
app.use(express.static('public'));
// set the view engine to ejs
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
console.log("hjh",__dirname, 'views');
app.get('/zz', (req, res) => {
    res.render('email-template', {email:"ss",message:"sd",username:"SSSS" })
}) 
app.listen(4000, () => {

    console.log("Server is listening on port 4000");
});